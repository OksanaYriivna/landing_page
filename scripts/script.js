const openDemoForm = document.querySelector('.js-demo-open-form');
const formInput = document.querySelectorAll('.form-input');
const form = document.querySelector('.registration-form');

formInput.forEach((input) => {
    input.addEventListener('focus', () => {
        input.parentElement.classList.add('form-input-area-onfocus');
        input.nextElementSibling.classList.add('input-name-onfocus');
    });
    input.addEventListener('blur', (onblur) => {
        if (input.value === '') {
            input.parentElement.classList.remove('form-input-area-onfocus');
            input.nextElementSibling.classList.remove('input-name-onfocus');
        } else {
            input.parentElement.classList.remove('form-input-area-onfocus');
        }
    });
});


document.querySelectorAll('.open-demo-button').forEach((item) => {
        item.addEventListener('click', () => {
            openDemoForm.style.display = "block";
        })
    }
);

form.addEventListener('submit', (event) => {
    event.preventDefault();
    openDemoForm.style.display = "none";
    document.querySelector(".js-make-request").style.display = "block";

});

document.querySelector('.form-close-button').addEventListener('click', (event) => {
    event.preventDefault();
    openDemoForm.style.display = "none";
});


document.querySelector('.confirm-message-button').addEventListener('click', () => {
    document.querySelector(".js-make-request").style.display = "none";
});

